class Restaurant < ApplicationRecord
  validates :name, :location, presence: true
#  has_many :users
  has_many :votes
  has_many :comments
  has_many :favorite_restaurants
  has_many :favorited_by, through: :favorite_restaurants, source: :user

  def self.up_vote restaurant, user
    Vote.create(:user_id => user.id, :restaurant_id => restaurant.id, :up_vote => 1, :down_vote => 0)
  end

  def self.down_vote restaurant, user
    Vote.create(:user_id => user.id, :restaurant_id => restaurant.id, :up_vote => 0, :down_vote => 1)
  end

  def get_total_up_votes
    self.votes.sum(:up_vote)
  end

  def get_total_down_votes
    self.votes.sum(:down_vote)
  end

  def self.search_by_name name
    if name 
      where('name LIKE ?', "%#{name}%")
    end
  end

  def self.search_by_location location
    if location 
      where('location LIKE ?', "%#{location}%")
    end
  end

  def self.leave_comment
    Comment.new
  end
end
