class Vote < ApplicationRecord 
  validates :up_vote, numericality: {greater_than_or_equal_to: 0}
  validates :down_vote, numericality: {greater_than_or_equal_to: 0}
  belongs_to :user
  belongs_to :restaurant
end
