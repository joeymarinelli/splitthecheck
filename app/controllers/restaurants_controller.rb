class RestaurantsController < ApplicationController
  before_action :set_restaurant, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show] 

  # GET /restaurants
  # GET /restaurants.json
  def index
      if params[:name]
        @restaurants = Restaurant.search_by_name(params[:name])
      elsif params[:location]
        @restaurants = Restaurant.search_by_location(params[:location])
      else
        @restaurants = Restaurant.all
      end
      @user = current_user
  end

  # GET /restaurants/1
  # GET /restaurants/1.json
  def show
  end

  # GET /restaurants/new
  def new
    @restaurant = Restaurant.new
  end

  # GET /restaurants/1/edit
  def edit
  end

  # POST /restaurants
  # POST /restaurants.json
  def create
    @restaurant = Restaurant.new(restaurant_params)
 #   @restaurant.up_votes = 0;
 #   @restaurant.down_votes = 0;
    respond_to do |format|
      if @restaurant.save
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully created.' }
        format.json { render :show, status: :created, location: @restaurant }
      else
        format.html { render :new }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /restaurants/1
  # PATCH/PUT /restaurants/1.json
  def update
    respond_to do |format|
      if @restaurant.update(restaurant_params)
        format.html { redirect_to @restaurant, notice: 'Restaurant was successfully updated.' }
        format.json { render :show, status: :ok, location: @restaurant }
      else
        format.html { render :edit }
        format.json { render json: @restaurant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /restaurants/1
  # DELETE /restaurants/1.json
  def destroy
    @restaurant.destroy
    respond_to do |format|
      format.html { redirect_to restaurants_url, notice: 'Restaurant was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def up_vote
    @restaurant = Restaurant.find(params[:restaurant_id])
    Restaurant.up_vote(@restaurant, current_user)
    redirect_to '/'
  end

  def down_vote
    @restaurant = Restaurant.find(params[:restaurant_id])
    Restaurant.down_vote(@restaurant, current_user)
    redirect_to '/'
  end

  def leave_comment
    Restaurant.leave_comment
    redirect_to @restaurant
  end

  def favorite 
    @restaurant = Restaurant.find(params[:restaurant_id])
    if current_user.favorites.include? @restaurant
      redirect_to '/', notice: 'You have already favorited ' + @restaurant.name
    else   
      current_user.favorites << @restaurant
      redirect_to '/', notice: 'You favorited ' + @restaurant.name
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_restaurant
      @restaurant = Restaurant.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def restaurant_params
      params.require(:restaurant).permit(:name, :location, :up_votes, :down_votes)
    end
end
