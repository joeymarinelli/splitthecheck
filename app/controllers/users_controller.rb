# frozen_string_literal: true

class UsersController < ApplicationController
  before_action only: [:show]
  before_action :authenticate_user!

  def show
    @user = current_user
  end
end
