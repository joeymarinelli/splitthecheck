require 'test_helper'

class RestaurantTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "up vote method increases number of upvotes by 1" do
    @user = users(:one)
    @restaurant = restaurants(:one)
    @vote = votes(:one)
    Restaurant.up_vote(@restaurant, @user)
    assert_equal(@restaurant.get_total_up_votes, 2)
    Restaurant.up_vote(@restaurant, @user)
    assert_equal(@restaurant.get_total_up_votes, 3)
  end

  test "down vote method increases number of upvotes by 1" do
    @user = users(:one)
    @restaurant = restaurants(:one)
    @vote = votes(:one)
    Restaurant.down_vote(@restaurant, @user)
    assert_equal(@restaurant.get_total_down_votes, 2)
    Restaurant.down_vote(@restaurant, @user)
    assert_equal(@restaurant.get_total_down_votes, 3)
  end

  test "restaurant name is required field" do
    restaurant = Restaurant.new(location:   "Test Location")
    assert restaurant.invalid?
    assert restaurant.errors[:name].any?
  end

  test "restaurant location is required field" do
    restaurant = Restaurant.new(name:   "Test Name")
    assert restaurant.invalid?
    assert restaurant.errors[:location].any?
  end

  test "should search restaurant by name" do
    all_restaurants = Restaurant.search_by_name("Test Restaurant")

    assert_equal(2, restaurants.size)
	restaurants = Restaurant.search_by_name("Test Restaurant One")

    assert_equal(1, restaurants.size)
    assert_equal(restaurants[0].name, "Test Restaurant One")
    assert_equal(restaurants[0].location, "Test Location One")
  end 

  test "should search restaurant by location" do
    all_restaurants = Restaurant.search_by_location("Test Locationn")

    assert_equal(2, restaurants.size)
	restaurants = Restaurant.search_by_location("Test Location Two")

    assert_equal(1, restaurants.size)
    assert_equal(restaurants[0].name, "Test Restaurant Two")
    assert_equal(restaurants[0].location, "Test Location Two")
  end 
end
