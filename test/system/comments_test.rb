require "application_system_test_case"

class CommentsTest < ApplicationSystemTestCase
  include Devise::Test::IntegrationHelpers
  setup do
    @comment = comments(:one)
    @user = users(:one)
    sign_in @user
  end

  test "visiting the index" do
    visit comments_url
    assert_selector "h1", text: "Comments"
  end

  test "creating a Comment" do
    visit comments_url
    click_on "New Comment"

    fill_in "Content", with: @comment.content
    fill_in "Restaurant", with: @comment.restaurant_id
    click_on "Create Comment"

    assert_text "Comment was successfully created"
    click_on "Back"
  end

end
