require "application_system_test_case"

class RestaurantsTest < ApplicationSystemTestCase
  include Devise::Test::IntegrationHelpers
  setup do
    @restaurant = restaurants(:one)
    @restaurant2 = restaurants(:two)
    @user = users(:one)
    sign_in @user
    @comment = comments(:one)
  end

  test "visiting the index" do
    visit restaurants_url
    assert_selector "h1", text: "Restaurants"
  end

  test "creating a Restaurant" do
    visit restaurants_url
    click_on "New Restaurant"

    fill_in "Location", with: @restaurant.location
    fill_in "Name", with: @restaurant.name
    click_on "Create Restaurant"

    assert_text "Restaurant was successfully created"
    click_on "Home"
  end

  test "updating a Restaurant" do
    visit restaurants_url
    click_on "Edit", match: :first
    @user.email = 'em@il.com'
    fill_in "Location", with: @restaurant.location
    fill_in "Name", with: @restaurant.name
    click_on "Update Restaurant"

    assert_text "Restaurant was successfully updated"
    click_on "Home"
  end

  test "search Restaurant by name" do 
    visit restaurants_url
    fill_in(id: "name", with: "Test restaurant one")
    click_button(value: "Search", id: "search_by_name")
    assert_selector("td", id: "name", text: "Test Restaurant One")
    assert_selector("td", id: "location", text: "Test Location One")
  end 

  test "search Restaurant by location" do 
    visit restaurants_url
    fill_in(id: "name", with: "Test location two")
    click_button(value: "Search", id: "search_by_location")
    assert_selector("td", id: "name", text: "Test Restaurant Two")
    assert_selector("td", id: "location", text: "Test Location Two")
  end

  test "upvoting a Restaurant" do
    visit restaurants_url
    click_button(value: "Will Split Check", match: :first)
    assert_selector("td", id: "up_votes", text: "2")
    click_button(value: "Will Split Check", match: :first)
    assert_selector("td", id: "up_votes", text: "3")
  end

  test "downvoting a Restaurant" do
    visit restaurants_url
    click_button(value: "Will Not Split Check", match: :first)
    assert_selector("td", id: "down_votes", text: "2")
    click_button(value: "Will Not Split Check", match: :first)
    assert_selector("td", id: "down_votes", text: "3")
  end

  test "favorite a Restaurant" do 
    visit restaurants_url
    click_button value: "Add to Favorites", match: :first
    assert_text "You favorited " + @restaurant2.name
  end

end
