require "application_system_test_case"

class VotesTest < ApplicationSystemTestCase
  include Devise::Test::IntegrationHelpers
  setup do
    @vote = votes(:one)
    @user = users(:one)
    sign_in @user
  end

  test "visiting the index" do
    visit votes_url
    assert_selector "h1", text: "Votes"
  end

end
