require "test_helper"
require "capybara"
require "helpers/chrome_helper"

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
    driven_by :headless_chrome
end
