require 'test_helper'

class VotesControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  setup do
    @vote = votes(:one)
    @user = users(:one)
    sign_in @user
  end

  test "should get index" do
    get votes_url
    assert_response :success
  end

  test "should get new" do
    get new_vote_url
    assert_response :success
  end

  test "should create vote" do
    assert_difference('Vote.count') do
      post votes_url, params: { vote: { down_vote: @vote.down_vote, restaurant_id: @vote.restaurant_id, up_vote: @vote.up_vote, user_id: @vote.user_id } }
    end

    assert_redirected_to vote_url(Vote.last)
  end

  test "should show vote" do
    get vote_url(@vote)
    assert_response :success
  end

  test "should redirect to sign in page when get vote and not signed in" do
    sign_out @user
    get votes_url
    assert_redirected_to '/users/sign_in'
  end
end
