require 'test_helper'

class RestaurantsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers
  
  setup do
    @restaurant = restaurants(:one)
    @restaurant2 = restaurants(:two)
    @user = users(:one)
    sign_in @user
  end

  test "should get index" do
    get restaurants_url
    assert_response :success
  end

  test "should get new" do
    get new_restaurant_url
    assert_response :success
  end

  test "should create restaurant" do
    assert_difference('Restaurant.count') do
      post restaurants_url, params: { restaurant: { location: @restaurant.location, name: @restaurant.name } }
    end
    assert_redirected_to restaurant_url(Restaurant.last)
  end

  test "should show restaurant" do
    get restaurant_url(@restaurant)
    assert_response :success
  end

  test "should get edit" do
    get edit_restaurant_url(@restaurant)
    assert_response :success
  end

  test "should update restaurant" do
    patch restaurant_url(@restaurant), params: { restaurant: { location: @restaurant.location, name: @restaurant.name } }
    assert_redirected_to restaurant_url(@restaurant)
  end

  test "should up vote restaurant" do
    put restaurant_up_vote_path(@restaurant, @user)
    assert_redirected_to '/'
  end

  test "should down vote restaurant" do 
    put restaurant_down_vote_path(@restaurant, @user)
    assert_redirected_to '/'
  end

  test "should redirect to sign in page when edit and not signed in" do
    sign_out @user
    get edit_restaurant_url(@restaurant)
    assert_redirected_to '/users/sign_in'
  end

  test "should redirect to sign in page when up vote restaurant and not signed in" do 
    sign_out @user
    put restaurant_up_vote_path(@restaurant, @user)
    assert_response :unauthorized
  end

  test "should redirect to sign in page when down vote restaurant and not signed in" do 
    sign_out @user
    put restaurant_down_vote_path(@restaurant, @user)
    assert_response :unauthorized
  end

  test "should redirect to sign in page when get new and not signed in" do
    sign_out @user
    get new_restaurant_url
    assert_redirected_to '/users/sign_in'
  end

  test "should add new favorite to user favorites" do
    put restaurant_favorite_path(@restaurant)
    assert_equal @user.favorites.count, 1
    put restaurant_favorite_path(@restaurant2)
    assert_equal @user.favorites.count, 2
  end

  test "should not add favorite that has already been added" do
    put restaurant_favorite_path(@restaurant)
    put restaurant_favorite_path(@restaurant)
    assert_equal @user.favorites.count, 1
  end

end
