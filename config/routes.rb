Rails.application.routes.draw do
  resources :comments
  resources :votes, except: [:destroy, :edit]
  devise_for :users, controllers: {
    sessions: 'users/sessions'
  }
  get 'user', to: 'users#show', as: 'user'
  
  resources :restaurants, except: :destroy do
    put :up_vote
    put :down_vote
    put :favorite
  end
  root 'restaurants#index', as: 'restaurants_index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
