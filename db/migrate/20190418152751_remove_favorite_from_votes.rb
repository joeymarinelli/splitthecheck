class RemoveFavoriteFromVotes < ActiveRecord::Migration[5.2]
  def change
    remove_column :votes, :favorite, :boolean
  end
end
