class CreateVotes < ActiveRecord::Migration[5.2]
  def change
    create_table :votes do |t|
      t.references :user, foreign_key: true
      t.references :restaurant, foreign_key: true
      t.integer :up_vote
      t.integer :down_vote

      t.timestamps
    end
  end
end
