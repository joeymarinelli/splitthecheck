class RemoveUpVotesFromRestaurants < ActiveRecord::Migration[5.2]
  def change
    remove_column :restaurants, :up_votes, :int
  end
end
