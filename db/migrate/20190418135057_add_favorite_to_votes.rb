class AddFavoriteToVotes < ActiveRecord::Migration[5.2]
  def change
    add_column :votes, :favorite, :boolean
  end
end
